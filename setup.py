"""Repear setup script"""

import io
import os
from setuptools import setup, find_packages

os.chdir(os.path.abspath(os.path.dirname(__file__)))


def read_requirements_in(path):
    """Read requirements from requirements.in file."""
    with io.open(path, 'rt', encoding='utf8') as f:  # pylint: disable=redefined-outer-name
        return [
            x.rsplit('=')[1] if x.startswith('-e') else x
            for x in [x.strip() for x in f.readlines()]
            if x
            if not x.startswith('-r')
            if not x[0] == '#'
        ]


INSTALL_REQUIRES = read_requirements_in('requirements.in')
EXTRAS_REQUIRE = {}
EXTRAS_REQUIRE['develop'] = read_requirements_in('requirements-develop.in')


setup(
    name="repear",
    version="0.1",
    packages=find_packages(),

    install_requires=INSTALL_REQUIRES,
    extras_require=EXTRAS_REQUIRE,
    setup_requires=['pytest-runner'],
    entry_points={
        'console_scripts': ['repear = repear.cli:repear']
    },
    tests_require=[
        'pytest',
        'pytest-cov',
        'testfixtures',
    ],
)
