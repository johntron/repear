"""The PairState class implementation"""

import csv


class PairState:
    """A PairState object"""
    def __init__(self, csv_path):
        self.devs = []
        self.pairs = []
        self.pair_map = {}
        self.filepath = csv_path
        with open(self.filepath) as csv_file:
            csv_reader = csv.reader(csv_file, delimiter=',')
            for pair in csv_reader:
                self.devs.append(pair[0])
                self.devs.append(pair[1])
                self.pairs.append(pair)
                self.pair_map[pair[0]] = pair[1]
                self.pair_map[pair[1]] = pair[0]

    def __str__(self):
        return str(self.pairs)

    def update(self, pairs):
        """Write the PairState to the file"""
        with open(self.filepath, 'w') as csvfile:
            writer = csv.writer(csvfile, delimiter=',', lineterminator='\n')
            for pair in pairs:
                writer.writerow(pair)
