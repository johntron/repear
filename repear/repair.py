"""
Finds new pair combinations given a history of pairs
"""

from statistics import median, mean
from typing import Set, Tuple, List
from networkx import Graph, complete_graph
from networkx.algorithms.operators.binary import compose
from networkx.algorithms.matching import max_weight_matching
from networkx.classes.function import set_edge_attributes

NodeLabel = str
Matching = Tuple[Tuple[str]]
Pairing = List[List[str]]


def complete(graph: Graph) -> Graph:
    """Fills in edges of graph to create a complete graph"""
    completed = complete_graph(graph)
    set_edge_attributes(completed, 0, 'weight')
    # preserve weights from original graph
    return compose(completed, graph)


def edge_attributes(graph: Graph, attribute) -> List:
    """Returns list of edge attributes"""
    return [a for (i, j, a) in graph.edges(data=attribute)]


def median_weight(graph: Graph) -> float:
    """Finds median weight of edges"""
    weights = edge_attributes(graph, 'weight')
    return median(weights)


def mean_weight(graph: Graph) -> float:
    """Finds mean weight of edges"""
    weights = edge_attributes(graph, 'weight')
    return mean(weights)


def min_weight(graph: Graph) -> int:
    """Finds min weight of edges"""
    weights = edge_attributes(graph, 'weight')
    return min(weights)


def max_weight(graph: Graph) -> int:
    """Finds max weight of edges"""
    weights = edge_attributes(graph, 'weight')
    return max(weights)


def set_computed_edge_attribute(graph: Graph, name: str, generator):
    """Stores new edge attributes using generator"""
    for (i, j, attributes) in graph.edges(data=True):
        graph.edges[i, j][name] = generator(attributes)


def increment_weight_from_matchings(graph: Graph, matchings: Set[Tuple[NodeLabel, NodeLabel]]):
    """Adds one to edge weight for each edge listed in matchings"""
    for (i, j) in matchings:
        graph.edges[i, j]['weight'] += 1


def graph_from(pair_matrix: dict) -> Graph:
    """Generates graph from repear PairMatrix dict"""
    graph = Graph()
    graph.add_nodes_from(pair_matrix.keys())

    for (node, pairings) in pair_matrix.items():
        for (pair, weight) in pairings.items():
            graph.add_edge(node, pair, weight=weight)
    return complete(graph)


def pairing_from_matching(matching: Matching) -> Pairing:
    """Converts networkx matchings to repear format"""
    return [list([u, v]) for (u, v) in matching]


def repair(graph: Graph) -> Matching:
    """Finds new matchings in graph using maximal-weight matching algorithm"""
    set_computed_edge_attribute(graph, 'inverted_weight', lambda edge: 0 - edge['weight'])
    return max_weight_matching(graph, maxcardinality=True, weight='inverted_weight')
