"""Tests for PairMatrix Class"""

import os
import shutil
import pytest


from repear.pair_matrix import PairMatrix, DuplicateName, NotInMatrix
from repear.cli import PAIR_MATRIX_FILENAME


@pytest.fixture(name='pair_matrix')
def load_pm(tmpdir):
    """Test fixture to create the PairMatrix"""
    # Run tests in the test directory
    cwd = os.path.abspath(os.path.dirname(__file__))
    pm_abspath = os.path.join(cwd, PAIR_MATRIX_FILENAME)
    shutil.copy(pm_abspath, tmpdir)
    tmp_pm_path = os.path.join(tmpdir, PAIR_MATRIX_FILENAME)
    return PairMatrix(tmp_pm_path)


def test_init(pair_matrix):
    """Test __init__ function"""
    assert os.path.basename(pair_matrix.filepath) == PAIR_MATRIX_FILENAME
    assert pair_matrix.data


def test_str(pair_matrix):
    """Test __str__ funciton"""
    expected = """name,foo,bar,baz,qux
foo,0,1,2,3
bar,1,0,3,4
baz,2,3,0,5
qux,3,4,5,0
"""
    assert str(pair_matrix) == expected


def test_add(pair_matrix):
    """Test add function"""
    with pytest.raises(DuplicateName):
        pair_matrix.add('foo')
    pair_matrix.add('foo2')
    assert pair_matrix.lookup('foo2', 'foo') == 0
    assert pair_matrix.lookup('foo2', 'foo2') == 0
    assert pair_matrix.lookup('foo2', 'foo2') == 0


def test_lookup(pair_matrix):
    """Test lookup function"""
    assert pair_matrix.lookup('foo', 'bar') == 1
    with pytest.raises(KeyError):
        pair_matrix.lookup('foo2', 'bar')


def test_remove(pair_matrix):
    """Test remove function"""
    pair_matrix.remove('foo')
    with pytest.raises(KeyError):
        pair_matrix.lookup('foo', 'bar')
    with pytest.raises(KeyError):
        pair_matrix.lookup('bar', 'foo')
    with pytest.raises(NotInMatrix):
        pair_matrix.remove('foo2')


def test_increment(pair_matrix):
    """Test increment function"""
    before = pair_matrix.lookup('bar', 'baz')
    pair_matrix.increment('bar', 'baz')
    assert pair_matrix.lookup('bar', 'baz') == before + 1


def test_write(pair_matrix):
    """Test write function"""
    expected = """name,foo,bar,baz,qux
foo,0,1,3,3
bar,1,0,3,4
baz,3,3,0,5
qux,3,4,5,0
"""
    pair_matrix.increment('foo', 'baz')
    pair_matrix.write()
    read_matrix = PairMatrix(pair_matrix.filepath)
    assert str(read_matrix) == expected


def test_update(pair_matrix):
    """Test update function"""
    expected = """name,foo,bar,baz,qux
foo,0,1,3,3
bar,1,0,3,4
baz,3,3,0,5
qux,3,4,5,0
"""
    before = pair_matrix.lookup('baz', 'foo')
    pair_matrix.update([['baz', 'foo']])
    assert pair_matrix.lookup('baz', 'foo') == before + 1
    read_matrix = PairMatrix(pair_matrix.filepath)
    assert str(read_matrix) == expected


def test_get_devs(pair_matrix):
    """Test get_devs function"""
    assert list(pair_matrix.get_devs()) == ['foo', 'bar', 'baz', 'qux']
