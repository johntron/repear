"""Tests for repair function"""

from networkx.readwrite.edgelist import parse_edgelist

from repear.repair import repair


def test_repair():
    """Test basic repair"""
    pairs = parse_edgelist([
        "Bob Linda 0",
        "Bob Tina 1",
    ], nodetype=str, data=(('weight', int),))
    pair_list = repair(pairs)
    assert ('Bob', 'Linda') in pair_list or ('Linda', 'Bob') in pair_list

    # I'm going to migrate some of the tests for CLI to this file to avoid the overhead and be
    # explicit about what we're testing

    # anchors = ['qux', 'bar']
    # sidekicks = set(['foo', 'baz'])
    # pair_list = repair(anchors, sidekicks, pair_matrix)
    # assert pair_list == [['bar', 'foo'], ['qux', 'baz']]

    # anchors = ['bar', 'qux']
    # sidekicks = set(['foo', 'baz'])
    # pair_list = repair(anchors, sidekicks, pair_matrix)
    # assert pair_list == [['bar', 'foo'], ['qux', 'baz']]
