"""The Pair Matrix class implementation"""

import csv
from collections import OrderedDict


class DuplicateName(Exception):
    """A name is duplicated in the pair matrix"""


class NotInMatrix(Exception):
    """A name is not in the pair matrix"""


class PairMatrix:
    """A PairMatrix object"""
    def __init__(self, csvname=None):
        self.filepath = csvname
        self.data = OrderedDict()
        with open(self.filepath) as csvfile:
            reader = csv.DictReader(csvfile)
            for row in reader:
                name = row['name']
                del row['name']
                unordered_data = {}
                for k, v in row.items():
                    if name != k:
                        unordered_data[k] = int(v)
                self.data[name] = OrderedDict(sorted(
                    unordered_data.items(), key=lambda t: t[1]
                ))

    def __str__(self):
        header = "name"
        str = ""
        for name in self.data.keys():
            header += f",{name}"
            str += f"{name}"
            for pair in self.data.keys():
                if name == pair:
                    str += ",0"
                else:
                    str += f",{self.data[name][pair]}"
            str += "\n"
        header += "\n"
        return header + str

    def write(self):
        """Write PairMatrix object to file"""
        with open(self.filepath, 'w', newline='') as csvfile:
            writer = csv.writer(csvfile, lineterminator='\n')
            writer.writerow(["name"] + list(self.data.keys()))
            for name in self.data.keys():
                row = [name]
                for pair in self.data.keys():
                    row += [self.data[name][pair] if name != pair else 0]
                writer.writerow(row)

    def get_devs(self):
        """Get list of developers in matrix"""
        return self.data.keys()

    def remove(self, rm_name):
        """Remove a developer from the PairMatrix"""
        if rm_name not in self.data.keys():
            raise NotInMatrix
        del self.data[rm_name]
        for name in self.data.keys():
            del self.data[name][rm_name]

    def add(self, new_name):
        """Add a developer to the PairMatrix"""
        if new_name in self.data.keys():
            raise DuplicateName
        self.data[new_name] = OrderedDict()
        for name in self.data.keys():
            self.data[new_name][name] = 0
            self.data[name][new_name] = 0

    def lookup(self, name1, name2):
        """Lookup how many times two developers have paired"""
        return self.data[name1][name2]

    def increment(self, name1, name2):
        """Increment by one the number of times two developers have paired"""
        self.data[name1][name2] += 1
        self.data[name2][name1] += 1

    def update(self, pairs):
        """Increment the number of times a pair of developers has paired"""
        for name1, name2 in pairs:
            self.increment(name1, name2)
        self.write()
