# -*- coding: utf-8 -*-
#
# Copyright 2017 - 2018  Ternaris.
# SPDX-License-Identifier: Apache-2.0

"""Utility functions"""

from os.path import exists, join
from subprocess import PIPE, run as _run


def run(cmdargs, *args, check=True, **kw):
    """Wrap subprocess.run"""
    cp = _run(cmdargs, *args, check=check, **kw)
    return cp


def runout(*args, **kw):
    """Run cmd and return its stdout."""
    return run(*args, **kw, stdout=PIPE).stdout.decode('utf-8')


def commit_update(msg, workdir):
    """Commit changes to PairMatrix and PairState CSVs"""
    commit_cmd = 'git commit -am'.split()
    commit_cmd.extend([msg])
    run(commit_cmd, cwd=workdir)


def init_repear_repo(workdir, pm_filename, ps_filename):
    """Initialize a repear repository"""
    run(f'touch {pm_filename}'.split(), cwd=workdir)
    run(f'touch {ps_filename}'.split(), cwd=workdir)
    run(f'git init'.split(), cwd=workdir)
    run(f'git add {pm_filename} {ps_filename}'.split(), cwd=workdir)
    commit_update('Initialize repear repository', workdir)


def is_valid_repear_repo(workdir, pm_filename, ps_filename):
    """ Check if a repear repository

    It must be a git repo with PairMatrix.csv and PairState.scv
    """
    return (exists(join(workdir, ps_filename))
            and exists(join(workdir, pm_filename))
            and exists(join(workdir, '.git')))
