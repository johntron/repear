"""The Repear CLI"""

import os
from datetime import date
import click

from repear.pair_state import PairState
from repear.pair_matrix import PairMatrix, NotInMatrix
from repear.repair import graph_from, repair
from repear.prompts import confirm_pairs, validate_matrix
from repear.utils import commit_update, init_repear_repo, is_valid_repear_repo

PAIR_MATRIX = None
PAIR_STATE = None
CWD = None

PAIR_MATRIX_FILENAME = 'PairMatrix.csv'
PAIR_STATE_FILENAME = 'PairState.csv'


@click.group()
@click.option('--workdir', default=os.curdir,
              help='Run repear in workdir',
              show_default=True)
def repear(workdir):
    """The Repear CLI"""
    global CWD
    global PAIR_MATRIX
    global PAIR_STATE
    CWD = workdir
    if not is_valid_repear_repo(workdir, PAIR_MATRIX_FILENAME, PAIR_STATE_FILENAME):
        click.confirm('A repear repository was not found. '
                      f'Would you like to create one in {os.path.abspath(CWD)}?',
                      abort=True,
                      default=False)
        init_repear_repo(CWD, PAIR_MATRIX_FILENAME, PAIR_STATE_FILENAME)
        print(f'A repear repository has been created in {os.path.abspath(CWD)}!')
    PAIR_STATE = PairState(os.path.join(workdir, PAIR_STATE_FILENAME))
    PAIR_MATRIX = PairMatrix(os.path.join(workdir, PAIR_MATRIX_FILENAME))


@repear.command('add')
@click.argument('name', required=True)
def repear_add(name):
    """Add a user to the pairing matrix"""
    print(f"Hello {name}!")
    PAIR_MATRIX.add(name)
    PAIR_MATRIX.write()
    if validate_matrix(PAIR_MATRIX):
        commit_update(f'Add {name} to pairing matrix', CWD)


@repear.command('remove')
@click.argument('name', required=True)
def repear_remove(name):
    """Remove a user from the pairing matrix"""
    print(f"Goodbye {name}!")
    try:
        PAIR_MATRIX.remove(name)
        PAIR_MATRIX.write()
        if validate_matrix(PAIR_MATRIX):
            commit_update(f'Remove {name} from pairing matrix', CWD)
    except NotInMatrix:
        pass


@repear.command('repair')
def repear_repair():
    """Generate a group of pairs based on:
       1. Who is anchoring each story
       2. Who is unavailable
       3. Who has paired the least
    """
    possible_pairs = graph_from(PAIR_MATRIX.data)
    pairs = repair(possible_pairs)

    # unavailable = prompt_for_unavailable(PAIR_MATRIX)
    # anchors, fixed_pairs = prompt_for_anchors(PAIR_MATRIX, PAIR_STATE)
    # fixed_devs = []
    # for devs in fixed_pairs:
    #     fixed_devs += devs
    # sidekicks = set(
    #     dev for dev in PAIR_MATRIX.get_devs() if dev not in anchors + unavailable + fixed_devs
    # )
    # if len(sidekicks) != len(anchors):
    #     sys.exit('Pairing is not possible. There are '
    #              f'{len(sidekicks)} sidekicks and {len(anchors)} anchors!')
    # pairs = fixed_pairs + anchors_and_sidekicks(anchors, sidekicks, PAIR_MATRIX)

    if confirm_pairs(pairs):
        # increment_weight_from_matchings(G, M)
        PAIR_STATE.update(pairs)
        PAIR_MATRIX.update(pairs)
        today = date.today().strftime('%Y-%m-%d')
        commit_update(f'Update pairs for {today}', CWD)
        print("Another day, another dollar!")
    else:
        print("We're stuck in yesteryear!")
